# 3.semesterProject
Robtek 3. Semester project

Projektoplæg
3 semester Robotteknologi
Efteråret 2015


Indledning:

Signalbehandling er en central kompetence for en civilingeniør i robotteknologi, idet robotter må
forventes at skulle udstyres med et stadigt stigende antal og typer af sensorer; det være sig
optiske, akustiske, elektriske, kameraer mv. Disse censorer vil være nødvendige dels for at styre,
manipulere og overvåge robottens bevægelser, dels for at tilgodese, at den kan løse sine konkrete
opgaver. For at understøtte denne og andre kompetencer er det ligeledes vigtigt, at en
civilingeniør i robotteknologi behersker et objektorienteret programmeringssprog og besidder
betydelig viden om datakommunikation og softwareudvikling.


Projektbeskrivelse:

Semesterprojektet har på mange måder en eksperimenterende tilgang, hvor det i høj grad er op til
jer selv, hvorledes løsningen skal skrues sammen.
Følgende ting ligger dog fast:

1. Bærbare computere skal kommunikere med hinanden, eller evt. et embedded system, ved udveksling af lyd.
2. Der skal anvendes DTMF‐toner, og der skal designes en kommunikationsprotokol.
3. Hertil skal der udvikles en distribueret applikation i C++.
4. Der skal anvendes en lagdelt softwarearkitektur
5. Arkitekturen kunne være client/server med f.eks. tykke klienter.


Det forventes, at applikationen udfører et eller andet meningsfyldt, men denne del er i høj grad op
til projektgruppen selv (det vil dog nok være en god idé at få løsningsforslaget godkendt af
projektvejlederen). Et eksempel kunne være, at serveren kontrollerer en chat mellem to klienter.
En anden mulighed kunne være, at serveren uddeler opgaver til klienterne (beregning,
fremskaffelse af data) og resultaterne af disse opgaver kommunikeres tilbage til serveren, som gør
et eller andet ved dem og præsenterer dem i en UI. Kommunikationen kunne også foregå med et
embedded system, som styrer f.eks. en lille robotbil. Husk dog det er en god idé, at jeres projekt
har en fornuftig færdiggørelsesgrad i forhold til det I planlægger at gennemføre, og at jeres
ambitionsniveau er realistisk.


Rapporten:

Rapporten skal indeholde dokumentation for den udførte del af arbejdet, såvel analyse som
begrundelse for de beslutninger, der er truffet undervejs, beskrivelse af de implementerede
løsninger herunder afprøvnings‐ og måleresultater. Rapporten omfang skal være på maks. 50 A4‐
sider, ekskl. bilag og kode, som evt. kan vedlægges på en CD. Eksaminationen tager udgangspunkt i
rapporten.


Rapporten skal afleveres i 2 eksemplarer senest fredag den 18. december kl. 12:00 i Ø28‐603D‐3
